SRC=$(wildcard *.tex)
TARGETDIR=out
TARGET=$(patsubst %.tex, ${TARGETDIR}/%.pdf, ${SRC})
DIR=${TARGETDIR}
#PDF=pdflatex -output-directory ${TARGETDIR} $<
PDF=latexmk

.PRECIOUS: ${SRC}

.PHONY: all
all: ${TARGET}


${TARGETDIR}/%.pdf: %.tex ${DIR} Makefile
	${PDF} $<


.PHONY: clean
clean:
	@find ./ -iregex ".*\.\(dvi\|log\|nav\|out\|ps\|snm\|toc\|aux\|cb\|tns\|bbl\|blg\|url\)" -delete


.PHONY: clean
mrpropre: clean
	rm -rf ${TARGETDIR}


${DIR}:
	@mkdir -p $@

